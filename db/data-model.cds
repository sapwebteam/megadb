namespace db;

entity Books {
  key ID : Integer;
  title  : String;
  stock  : Integer;
}

entity Author {
  key ID : Integer;
  name  : String;
  surname  : Integer;
}
entity Order {
  key ID : Integer;
  vbeln  : String;
  kunnr  : String;
}