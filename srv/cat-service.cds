using db as my from '../db/data-model';

service CatalogService {
    entity Books as projection on my.Books;
    entity Authors as projection on my.Author;
    entity Orders as projection on my.Order;
}